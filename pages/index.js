import Head from 'next/head';
import LandingPage from '../components/LandingPage/LandingPage';

export default function Home() {
  return (
    <div>
      <Head>
        <title>Envited</title>
        <meta name="description" content="Envited" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <LandingPage />
    </div>
  )
}
