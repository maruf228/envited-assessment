import Head from 'next/head';
import CreatePage from '../components/CreatePage/CreatePage';

export default function Create() {
  return (
    <div>
      <Head>
        <title>Create Event</title>
        <meta name="description" content="Create Event Page" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <CreatePage />
    </div>
  )
}
