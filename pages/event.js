import Head from 'next/head';
import EventPage from '../components/EventPage/EventPage';

export default function Event() {
  return (
    <div>
      <Head>
        <title>Event</title>
        <meta name="description" content="Event Page" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <EventPage />
    </div>
  )
}
