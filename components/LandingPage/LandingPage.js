import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './landingPage.module.css';

export default function LandingPage() {
  const router = useRouter();
  return (
    <div className={styles.container}>
      <div className={styles.motoTextWrapper}>
        <div>Imagine if</div>
        <div className={styles.snapChatText}>Snapchat</div>
        <div>had events.</div>
      </div>

      <p className={styles.paragraph}>
        Easily host and share events with your friends across any social media.
      </p>

      <div className={styles.eventPreviewImageWrapper}>
        <Image
          src="/assets/images/event-image.svg"
          alt="Landing Image"
          width={165}
          height={292}
          layout="responsive"
        />
      </div>

      <button className={styles.createEventBtn}>
        <Link href='/create'>
          <a>        
            🎉 Create my event
          </a>
        </Link>
      </button>
    </div>
  )
}