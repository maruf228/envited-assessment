import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './createPage.module.css';

export default function CreatePage() {
  const router = useRouter();
  return (
    <div className={styles.container}>
      <div className={styles.pageTitle}>Create An Event</div>
      
      <div className={styles.perInput}>
        <div className={styles.label}>Event Name</div>
        <div>
          <input
            type="text"
            className={styles.textInput}
          />
        </div>
      </div>

      <div className={styles.perInput}>
        <div className={styles.label}>Host Name</div>
        <div>
          <input
            type="text"
            className={styles.textInput}
          />
        </div>
      </div>

      <div className={styles.perInput}>
        <div className={styles.label}>Event Start Time</div>
        <div>
          <input
            type="datetime-local"
            className={styles.textInput}
          />
        </div>
      </div>

      <div className={styles.perInput}>
        <div className={styles.label}>Event End Time</div>
        <div>
          <input
            type="datetime-local"
            className={styles.textInput}
          />
        </div>
      </div>

      <div className={styles.perInput}>
        <div className={styles.label}>Location</div>
        <div>
          <input
            type="text"
            className={styles.textInput}
          />
        </div>
      </div>

      <div className={styles.perInput}>
        <div className={styles.label}>Event Photo</div>
        <div>
          <input
            type="file"
            className={styles.textInput}
          />
        </div>
      </div>

      <button className={styles.nextBtn}>
        <Link href='/event'>
          <a>        
            Next Page
          </a>
        </Link>
      </button>
        
    </div>
  )
}