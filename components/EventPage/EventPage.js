import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './eventPage.module.css';
import { BiCalendar } from 'react-icons/bi';
import { HiOutlineLocationMarker } from 'react-icons/hi';
import { MdOutlineKeyboardArrowRight } from 'react-icons/md';

export default function EventPage() {
  const router = useRouter();
  return (
    <div className={styles.container}>
      <div className={styles.imageWrapper}>
        <Image
          src="/assets/images/cake-image.png"
          alt="Event Image"
          width={1080}
          height={1080}
          layout="responsive"
        />
      </div>

      <div className={styles.infoSection}>
        <div className={styles.eventTitleText}>Birthday Bash</div>
        <div className={styles.hostedByText}>Hosted by <span>Elysia</span></div>

        <div className={styles.infoIconTextWrapper}>
          <div className={styles.iconWrapper}>
            <BiCalendar size={20} />
          </div>

          <div className={styles.infoArrowWrapper}>
            <div>
              <div className={styles.eventStartTimeText}>18 August 6:00PM</div>
              <div className={styles.eventEndTimeText}>to <b>19 August 1:00PM</b> UTC +10</div>
            </div>

            <MdOutlineKeyboardArrowRight size={20} />
          </div>
        </div>

        <div className={styles.infoIconTextWrapper}>
          <div className={styles.iconWrapper}>
            <HiOutlineLocationMarker size={20} />
          </div>

          <div className={styles.infoArrowWrapper}>
            <div>
              <div className={styles.eventStartTimeText}>Street Name</div>
              <div className={styles.eventEndTimeText}>Suburb, State, Postcode</div>
            </div>

            <MdOutlineKeyboardArrowRight size={20} />
          </div>
        </div>
      </div>
    </div>
  )
}